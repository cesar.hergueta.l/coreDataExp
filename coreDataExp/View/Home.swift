//
//  Home.swift
//  coreDataExp
//
//  Created by César Hergueta on 03-07-22.
//

import SwiftUI

struct Home: View {
    
    @StateObject var model = ViewModel()
    @FetchRequest(
        entity: Notas.entity(),
        sortDescriptors:
            [NSSortDescriptor(key: "fecha", ascending: true)],
        animation: .spring()
    ) var results : FetchedResults<Notas>
    
    @Environment(\.managedObjectContext) var context
    
    var body: some View {
        NavigationView {
            List {
                ForEach(results) { item in
                    VStack(alignment: .leading) {
                        Text(item.nota ?? "Sin nota")
                            .font(.title)
                            .bold()
                        Text(item.fecha ?? Date(), style: .date)
                    }
                    .contextMenu(menuItems: {
                        Button(
                            action: {
                                model.viewData(item: item)
                            },
                            label: {
                                Text("Editar")
                                Image(systemName: "pencil")
                            }
                        )
                        Button(
                            action: {
                                model.deleteData(item: item, context: context)
                            },
                            label: {
                                Text("Eliminar")
                                Image(systemName: "trash")
                            }
                        )
                    })
                }
            }
            .navigationTitle("Notas")
            .toolbar(content : {
                Button(action: {
                    model.show.toggle()
                }, label: {
                    Image(systemName: "plus")
                        .font(.title)
                        .foregroundColor(.blue)
                })
            })
            .sheet(isPresented: $model.show, content: { appView(model: model) })
        }
    }
}
