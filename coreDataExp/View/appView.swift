//
//  appView.swift
//  coreDataExp
//
//  Created by César Hergueta on 03-07-22.
//

import SwiftUI

struct appView: View {
    
    @ObservedObject var model: ViewModel
    @Environment(\.managedObjectContext) var context
    
    
    var body: some View {
        VStack {
            Text( model.updateItem == nil ? "Agregar nota" : "Editar nota")
                .font(.largeTitle)
                .bold()
            Spacer()
            TextEditor(text: $model.nota)
            Divider()
            DatePicker("Seleccionar fecha", selection: $model.fecha)
            Spacer()
            Button(action: {
                if model.updateItem != nil {
                    model.editData(context: context)
                } else {
                    model.saveData(context: context)
                }
            }){
                Label(
                    title: {
                        Text("Guardar").foregroundColor(.white).bold()
                    },
                    icon: {
                        Image(systemName: "plus").foregroundColor(.white)
                    }
                )
            }
            .padding()
            .frame(width: UIScreen.main.bounds.width - 30)
            .background(model.nota != "" ? Color.blue : Color.gray)
            .cornerRadius(8)
            .disabled(model.nota != "" ? true : false)
        }.padding()
    }
}
