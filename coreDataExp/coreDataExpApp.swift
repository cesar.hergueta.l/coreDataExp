//
//  coreDataExpApp.swift
//  coreDataExp
//
//  Created by César Hergueta on 03-07-22.
//

import SwiftUI

@main
struct coreDataExpApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
